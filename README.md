UR Baja SAE Datalogger
======
##### Hardware  
  * Arduino Uno  
  * Adafruit Datalogging Shield  
  * ADXL345 Accelerometer  
  * L3GD20 Gyroscope  
  * Two ATtiny85 slaves  
  * Two MLX90614 themometers (0x5A and 0x5B)  
  
##### Pins  
  * All sensors share I2C bus  
  * Use 4.7k pull-up resistors for I2C bus  

##### Original author  
  * David E. Gonzalez Garcia (david+baja@wargh.org)  

Transition notes
------

Not much really, just make sure you tell every relevant PTL to finish their stuff at least a week **before** competition as to avoid rushing wiring. Talking of which, remember to use heatshrinking crimp connectors whenever possible. Oh and remind the current president to remember to pay for the rochesterbaja.com renewal on Namecheap.com.
