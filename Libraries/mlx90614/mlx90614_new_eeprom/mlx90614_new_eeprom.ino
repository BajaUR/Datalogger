#include <i2cmaster.h>

void setup(){
	Serial.begin(9600);
	Serial.println("Press any to begin...");

	while (Serial.available() == 0) {}
	
	Serial.println("Starting...");

	i2c_init();
	PORTC = (1 << PORTC4) | (1 << PORTC5); 

	int dev = 0x5B; 
	unsigned int data_l = 0;
	unsigned int data_h = 0;
	int pec = 0;
	float data_t = 0;
	float emissivity = 0;

	//WRITE TO EEPROM, FIRST: ERASE OLD STUFF
	Serial.println("1: Erasing old EEPROM settings...");
	i2c_start_wait(dev+I2C_WRITE);
	i2c_write(0x25);
	i2c_write(0x00); //Erase low byte (write 0)
	i2c_write(0x00); //Erase high byte (write 0)
	i2c_write(0x83); //Send PEC
	//For PEC Calculation have a look at : https://ghsi.de/CRC/index.php?Polynom=100000111&Message=040000
	//In this case the PEC calculates from 250000 (=0x83)
	i2c_stop();
	delay(5000);

	/*
	Serial.println("Erasing maximum temperature setting...");
	i2c_start_wait(dev+I2C_WRITE);
	i2c_write(0x20);
	i2c_write(0x00);
	i2c_write(0x00);
	i2c_write(0x43);
	i2c_stop();
	delay(5000);

	Serial.println("Erasing minimum temperature setting...");
	i2c_start_wait(dev+I2C_WRITE);
	i2c_write(0x21);
	i2c_write(0x00);
	i2c_write(0x00);
	i2c_write(0x28); 
	i2c_stop();
	delay(5000);
	*/
	Serial.println("Erasing emissivity setting...");
	i2c_start_wait(dev+I2C_WRITE);
	i2c_write(0x04);
	i2c_write(0x00); 
	i2c_write(0x00);
	i2c_write(0xAB);
	i2c_stop();

	//WRITE TO EEPROM, THE NEW STUFF!
	Serial.println("2: Write new settings to EEPROM...");
	i2c_start_wait(dev+I2C_WRITE);
	i2c_write(0x25); //Register Address to write to
	i2c_write(0x74); //New filter settings (B374)
	i2c_write(0xB3);
	i2c_write(0x65); //Send PEC
	i2c_stop();
	delay(5000);
	/*
	Serial.println("Writing new maximum temperature setting...");
	i2c_start_wait(dev+I2C_WRITE);
	i2c_write(0x20);
	i2c_write(0xFF);
	i2c_write(0xFF);
	i2c_write(0x67);
	i2c_stop();
	delay(5000);

	Serial.println("Writing new minimum temperature setting...");
	i2c_start_wait(dev+I2C_WRITE);
	i2c_write(0x21);
	i2c_write(0x5B); 
	i2c_write(0x4F);
	i2c_write(0x59);
	i2c_stop();
	*/
	Serial.println("Writing new emissivity setting...");
	i2c_start_wait(dev+I2C_WRITE);
	i2c_write(0x04);
	i2c_write(0x2E); // dec2hex(round(65535*emissivity))
	i2c_write(0x14); // emissivity = 0.18 for aluminum
	i2c_write(0xBF);
	i2c_stop();

	Serial.println("");
	Serial.println("----------Finish!----------");
}
void loop(){
}
