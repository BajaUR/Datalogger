Master datalogger settings
======
There are three setting blocks within the master datalogger. The first one, allows you to enable or disable logging various sensors.
```c
// Sensors to log (0: OFF, 1: ON) ///////////////////////////////////////////////////
#define HALL1               1           // 0x10 (gearbox)
#define HALL2               1           // 0x11 (engine)
#define ACCEL               0
#define GYRO                0
#define TEMP1               0           // 0x5A
#define TEMP2               0           // 0x5B
#define SD_CARD_ENABLE      1
```
Second, is where we define the baud rate of the serial connection, if we wish to echo the logged data to serial (so you can watch data live on serial monitor), and finally, if you wish to wait to start logging (you must enter any character in the serial monitor to start logging).  
```c
// Startup settings /////////////////////////////////////////////////////////////////
#define SERIAL_BAUD         9600        // Serial
#define ECHO_TO_SERIAL      1           // Echo data to serial port
#define WAIT_TO_START       0           // Wait for serial input in setup()
#define MLX90614_TA         0x06
#define MLX90614_TOBJ1      0x07
```
And finally, the SD card settings (note that they still apply --even-- if you disable `SD_CARD_ENABLE` in the first block), which control the rate (in milliseconds) at which we poll the various sensors and how often we flush into the SD card. This rate should be high enough we get all the minute details but slow enough that we give the sensors enough time to process our requests.
```c
// SD card settings /////////////////////////////////////////////////////////////////
#define LOG_INTERVAL        20
#define SYNC_INTERVAL       10*LOG_INTERVAL
```
