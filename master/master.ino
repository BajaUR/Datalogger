/*

UofR Baja SAE master

Author:		David E. Gonzalez Garcia
Revision:	2.3.4 (04/10/2016)
Hardware:   Arduino Uno // Teensy 3.2
			Adafruit Datalogging Shield
			ADXL345 Accelerometer
			L3GD20 Gyroscope
			Two ATtiny85 slaves
			Two MLX90614 themometers (0x5A and 0x5B)

Pins:   	All sensors share I2C bus
			Use 4.7k pull-up resistors for I2C bus
      
*/

// Libraries ////////////////////////////////////////////////////////////////////////
#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <RTClib.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_L3GD20_U.h>
#include <Adafruit_ADXL345_U.h>

// Sensors to log (0: OFF, 1: ON) ///////////////////////////////////////////////////
#define HALL1               1 // 0x10 (gearbox)
#define HALL2               1 // 0x11 (engine)
#define ACCEL               0
#define GYRO                0
#define TEMP1               0 // 0x5A
#define TEMP2               0 // 0x5B
#define SD_CARD_ENABLE		1

// Startup settings /////////////////////////////////////////////////////////////////
#define SERIAL_BAUD         9600       // Serial
#define ECHO_TO_SERIAL      1    // Echo data to serial port
#define WAIT_TO_START       0    // Wait for serial input in setup()
#define MLX90614_TA         0x06
#define MLX90614_TOBJ1      0x07

// SD card settings /////////////////////////////////////////////////////////////////
#define LOG_INTERVAL			20
#define SYNC_INTERVAL			10*LOG_INTERVAL
uint32_t syncTime = 0;
const int chipSelect = 10;

// Miscellaneous ////////////////////////////////////////////////////////////////////
File logfile;
RTC_Millis rtc;
Adafruit_L3GD20_Unified gyro = Adafruit_L3GD20_Unified(20);
Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);

boolean rtcOn = true;

void setup(){
	Serial.begin(SERIAL_BAUD);

	#if SD_CARD_ENABLE
		Serial.print("Initializing SD card... ");
		pinMode(10, OUTPUT);
		if (!SD.begin(chipSelect)) {
			error("NONE DETECTED");
		}

		char filename[] = "LOGGER00.CSV";
		for (uint8_t i = 0; i < 100; i++) {
			filename[6] = i/10 + '0';
			filename[7] = i%10 + '0';
			if (! SD.exists(filename)) {
				logfile = SD.open(filename, FILE_WRITE); 
				break;
			}
		}

		if (! logfile) {
			error("UNABLE TO WRITE");
		}

		Serial.println("DONE");
	#endif

	// Initialise I2C library
	Wire.begin();

	Serial.print("Initializing RTC... ");
		//rtc.begin();
		rtc.begin(DateTime(F(__DATE__), F(__TIME__)));
		/*if (!rtc.isrunning()) {
		#if SD_CARD_ENABLE
			  logfile.println("No RTC");
		#endif
		  rtcOn = false;
		#if ECHO_TO_SERIAL
		  Serial.print("FAIL");
		#endif
		}*/
	Serial.println("DONE");

	#if ACCEL
		Serial.print("Initializing Accelerometer... ");
		// Initialise the sensor
		if(!accel.begin()){
			error("NONE DETECTED");
		}
		accel.setRange(ADXL345_RANGE_8G);
		accel.setDataRate(ADXL345_DATARATE_50HZ);
		Serial.println("DONE");
	#endif

	#if GYRO
		Serial.print("Initializing Gyroscope... ");
		if (!gyro.begin(gyro.L3DS20_RANGE_2000DPS)){
			error("NONE DETECTED");
		}
		Serial.println("DONE");
	#endif

	Serial.print("System compiled on: ") Serial.print(__DATE__); Serial.print(F(" ")); Serial.println(__TIME__);
	Serial.println("System boot complete.");
	
	// Print header /////////////////////////////////////////////////////////////////////
	
	#if SD_CARD_ENABLE
		Serial.print("Log file: "); Serial.println(filename);

		logfile.print("millis");

		if (rtcOn){
			logfile.print(",stamp");
		}

		#if HALL1
			logfile.print(",rpm1");
		#endif

		#if HALL2
			logfile.print(",rpm2");
		#endif

		#if ACCEL
			logfile.print(",filtered.XAxis,filtered.YAxis,filtered.ZAxis,rollAcc,pitchAcc");
		#endif

		#if GYRO
			logfile.print(",gyro.x,gyro.y,gyro.z");
		#endif

		#if TEMP1
			logfile.print(",ambient1_C,object1_C");
		#endif

		#if TEMP2
			logfile.print(",ambient2_C,object2_C");
		#endif

		logfile.println("");
	#endif

	#if ECHO_TO_SERIAL
		Serial.print("millis");

		if (rtcOn){
			Serial.print(",stamp");
		}

		#if HALL1
			Serial.print(",rpm1");
		#endif

		#if HALL2
			Serial.print(",rpm2");
		#endif

		#if ACCEL
			Serial.print(",filtered.XAxis,filtered.YAxis,filtered.ZAxis,rollAcc,pitchAcc");
		#endif

		#if GYRO
			Serial.print(",gyro.x,gyro.y,gyro.z");
		#endif

		#if TEMP1
			Serial.print(",ambient1_C,object1_C");
		#endif

		#if TEMP2
			Serial.print(",ambient2_C,object2_C");
		#endif

		Serial.println("");
	#endif

	#if WAIT_TO_START
		Serial.println("Type any character to start");
		while (!Serial.available());
	#endif
}

void loop(){
	uint8_t a,b,c,d;
	unsigned long rpm1;
	unsigned long rpm2;
	delay((LOG_INTERVAL -1) - (millis() % LOG_INTERVAL));

	#if HALL1
		/*
		// Old requestFrom handler
		Wire.requestFrom(0x10, 2);
		while (Wire.available()){
		  if (firstByte1){
			lowByte1 = Wire.read();
			firstByte1 = false;
		  } else {
			highByte1 = Wire.read();
			firstByte1 = true;
		  }
		}
		rpm1 = ((highByte1 << 8) + lowByte1);
		*//////////////////////////
		Wire.requestFrom(0x10, 2);
		a = Wire.read();
		b = Wire.read();
		rpm1 = a;
		rpm1 = (rpm1 << 8) | b;
		/*
		Wire.requestFrom(0x10, 4);
		a = Wire.read();
		b = Wire.read();
		c = Wire.read();
		d = Wire.read();
		rpm1 = a;
		rpm1 = (rpm1 << 8) | b;
		rpm1 = (rpm1 << 8) | c;
		rpm1 = (rpm1 << 8) | d;
		*/
	#endif

	#if HALL2
		/*
		// Old requestFrom handler
		Wire.requestFrom(0x11, 2);
		while (Wire.available()){
		  if (firstByte2){
			lowByte2 = Wire.read();
			firstByte2 = false;
		  } else {
			highByte2 = Wire.read();
			firstByte2 = true;
		  }
		}
		rpm2 = ((highByte2 << 8) + lowByte2);
		*/////////////////////////
		Wire.requestFrom(0x11, 2);
		a = Wire.read();
		b = Wire.read();
		rpm2 = a;
		rpm2 = (rpm2 << 8) | b;
		/*
		Wire.requestFrom(0x11, 4);
		a = Wire.read();
		b = Wire.read();
		c = Wire.read();
		d = Wire.read();
		rpm2 = a;
		rpm2 = (rpm2 << 8) | b;
		rpm2 = (rpm2 << 8) | c;
		rpm2 = (rpm2 << 8) | d;
		*/
	#endif

	#if ACCEL
		Vector norm = accel.readNormalize();
		Vector filtered = accel.lowPassFilter(norm, 0.5);
	#endif

	#if GYRO
		gyro.read();
	#endif

	#if ACCEL
		float rollAcc = (atan2(filtered.YAxis, filtered.ZAxis)*180.0)/M_PI;
		float pitchAcc = -(atan2(filtered.XAxis, sqrt(filtered.YAxis*filtered.YAxis + filtered.ZAxis*filtered.ZAxis))*180.0)/M_PI;
	#endif

	#if TEMP1
		float ambient1 = readAmbientTempC(0x5A);
		float object1  =  readObjectTempC(0x5A);
	#endif

	#if TEMP2
		float ambient2 = readAmbientTempC(0x5B);
		float object2  =  readObjectTempC(0x5B);
	#endif
	
	DateTime now;
	now = rtc.now();
	uint32_t m = millis();
	#if SD_CARD_ENABLE
		logfile.print(m);
		if (rtcOn){
			logfile.print(",");  
			logfile.print(now.unixtime());
		}
		#if HALL1
			logfile.print(",");
			logfile.print(rpm1);
		#endif
		#if HALL2
			logfile.print(",");
			logfile.print(rpm2);
		#endif
		#if ACCEL
			logfile.print(",");
			logfile.print(filtered.XAxis); 
			logfile.print(",");
			logfile.print(filtered.YAxis); 
			logfile.print(",");
			logfile.print(filtered.ZAxis);
			logfile.print(",");
			logfile.print(rollAcc);
			logfile.print(",");
			logfile.print(pitchAcc);
		#endif
		#if GYRO
			logfile.print(",");
			logfile.print((int)gyro.data.x); 
			logfile.print(",");
			logfile.print((int)gyro.data.y); 
			logfile.print(",");
			logfile.print((int)gyro.data.z);
		#endif
		#if TEMP1
			logfile.print(",");
			logfile.print(ambient1);
			logfile.print(",");
			logfile.print(object1);
		#endif
		#if TEMP2
			logfile.print(",");
			logfile.print(ambient2);
			logfile.print(",");
			logfile.print(object2);
		#endif
		logfile.println("");
	#endif

	#if ECHO_TO_SERIAL
		Serial.print(m);
		if (rtcOn){
			Serial.print(",");  
			Serial.print(now.unixtime());
		}
		#if HALL1
			Serial.print(",");
			Serial.print(rpm1);
		#endif
		#if HALL2
			Serial.print(",");
			Serial.print(rpm2);
		#endif
		#if ACCEL
			Serial.print(",");
			Serial.print(filtered.XAxis); 
			Serial.print(",");
			Serial.print(filtered.YAxis); 
			Serial.print(",");
			Serial.print(filtered.ZAxis);
			Serial.print(",");
			Serial.print(rollAcc);
			Serial.print(",");
			Serial.print(pitchAcc);
		#endif
		#if GYRO
			Serial.print(",");
			Serial.print((int)gyro.data.x); 
			Serial.print(",");
			Serial.print((int)gyro.data.y); 
			Serial.print(",");
			Serial.print((int)gyro.data.z);
		#endif
		#if TEMP1
			Serial.print(",");
			Serial.print(ambient1);
			Serial.print(",");
			Serial.print(object1);
		#endif
		#if TEMP2
			Serial.print(",");
			Serial.print(ambient2);
			Serial.print(",");
			Serial.print(object2);
		#endif
		Serial.println("");
	#endif

	// Now we write data to disk! Don't sync too often - requires 2048 bytes of I/O to SD card
	// which uses a bunch of power and takes time
	if ((millis() - syncTime) < SYNC_INTERVAL) return;
	syncTime = millis();
	
	#if SD_CARD_ENABLE
		digitalWrite(2, HIGH);
		logfile.flush();
		digitalWrite(2, LOW);
	#endif
}

void error(const char *str){
	Serial.print("FAIL: ");
	Serial.println(str);
	while(1);
}

// MLX90614 Functions ///////////////////////////////////////////////////////////////
double readObjectTempC(uint8_t i2caddr) {
	return readTemp(MLX90614_TOBJ1, i2caddr);
}

double readAmbientTempC(uint8_t i2caddr) {
	return readTemp(MLX90614_TA, i2caddr);
}

float readTemp(uint8_t reg, uint8_t i2caddr) {
	float temp;
	temp = read16_mlx(reg, i2caddr);
	temp *= .02;
	temp  -= 273.15;
	return temp;
}

uint16_t read16_mlx(uint8_t a, uint8_t i2caddr) {
	uint16_t ret;
	Wire.beginTransmission(i2caddr);    // start transmission to device 
	Wire.write(a);                      // sends register address to read from
	Wire.endTransmission(false);        // end transmission

	Wire.requestFrom(i2caddr, (uint8_t)3);  // send data n-bytes read
	ret  = Wire.read();           // receive DATA
	ret |= Wire.read() << 8;      // receive DATA
	// Don't need pec, but gotta read it
	uint8_t pec = Wire.read();
	return ret;
}
