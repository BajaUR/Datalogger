ATtiny85 Slave Code
======

This one is easy, just uncomment the lines as required by whichever gear we are measuring. So for example, if we wish to measure the gearbox, uh, gear, we must uncomment as follows:
```c
// Gearbox //////////////////////////////////////////////////
#define I2C_SLAVE_ADDR  0x10
int wheel_ticks = 64;

// Engine //////////////////////////////////////////////////
//#define I2C_SLAVE_ADDR  0x11
//int wheel_ticks = 44;
```
Alternatively, if we wish to measure the tonewheel that is attached on the engine output shaft, we comment the lines for the gearbox and uncomment those for the car, as shown below:
```c
// Gearbox //////////////////////////////////////////////////
//#define I2C_SLAVE_ADDR  0x10
//int wheel_ticks = 64;

// Engine //////////////////////////////////////////////////
#define I2C_SLAVE_ADDR  0x11
int wheel_ticks = 44;
```
I strongly recommend that you do not change the `I2C_SLAVE_ADDR` lines unless you wish to add more Hall effect sensors; remember, i2c devices must have a unique address. Same thing with the integer `wheel_ticks`.  
## How to upload code to ATTiny85's?
Easy, just follow the instructions found on [this website](http://highlowtech.org/?p=1695).
## It doesn't work!
Kay, just change `rpm_array[1] = 60000000/(time*wheel_ticks);` to `rpm_array[1] = 60*(1000000/((float)time*wheel_ticks));` then go shout at Prof. Gracewski because she proposed that change and I couldn't test it.
