/*

UofR Baja SAE ATtiny85 slave

Author:		David Gonzalez Garcia
Hardware: 	ATtiny85
			Cherrycorp Hall Effect sensors
Pins:
		1: RESET  8: VCC
		2: PB3    7: I2C SCL
		3: PB4    6: PB1 << Hall Effect input
		4: GND    5: I2C SDA
		
		Use 1k pull-ups resistors for Hall Effect sensors.
*/

#include <PinChangeInterrupt.h>
#include <TinyWireS.h>
#define in_pin PB1

// Gearbox //////////////////////////////////////////////////
#define I2C_SLAVE_ADDR  0x10
int wheel_ticks = 64;

// Engine //////////////////////////////////////////////////
//#define I2C_SLAVE_ADDR  0x11
//int wheel_ticks = 44;

#ifndef TWI_RX_BUFFER_SIZE
	#define TWI_RX_BUFFER_SIZE ( 16 )
#endif

volatile unsigned long time = 0;
volatile unsigned long time_last = 0;
volatile unsigned int  rpm_array[2] = {0,0};
volatile unsigned int  rpm = 0;

uint8_t rpm_i2c[2];
const uint8_t reg_size = sizeof(rpm_i2c);

void setup(){
	// Set PB1 as input
	pinMode(in_pin, INPUT);
	digitalWrite(in_pin, HIGH);
	// Initialize i2c
	TinyWireS.begin(I2C_SLAVE_ADDR);
	TinyWireS.onRequest(requestEvent);
	// Attach PCInt to PB1
	attachPcInterrupt(in_pin, sensor_isr, RISING);
}

void loop(){
	// TinyWireS needs to stop and check if we have received a request
	TinyWireS_stop_check();
}

void update_rpm(){
	//2 sample moving average on rpm readings
	// 60 seconds to a minute // 1,000,000 microseconds in a second
	detachPcInterrupt(in_pin);
	rpm_array[0] = rpm_array[1];
	rpm_array[1] = 60000000/(time*wheel_ticks);
	rpm = (rpm_array[0] + rpm_array[1]) / 2;
	attachPcInterrupt(in_pin, sensor_isr, RISING);
}


void requestEvent(){
	TinyWireS.send(uint8_t[reg_position]);
	reg_position++;
	if (reg_position >= reg_size){
		update_rpm();
		// We must divide the 16-bit integer into two 8-bit bytes.
		rpm_i2c[0] = (rpm >> 8) & 0xff; // Shift the 16-bit integer by 8 bits, mask the rest
		rpm_i2c[1] = rpm & 0xff;		// Get the rest of the integer
    }
}

void sensor_isr(){
	time = (micros() - time_last);
	time_last = micros();
}
